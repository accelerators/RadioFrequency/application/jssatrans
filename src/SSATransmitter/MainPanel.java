/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SSATransmitter;

import RFTools.SeqHistoryPanel;
import SSAAmpli.TransistorAlarmDialog;
import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.AttributeStateEvent;
import fr.esrf.tangoatk.core.CommandList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.DevStateScalarEvent;
import fr.esrf.tangoatk.core.ErrorEvent;
import fr.esrf.tangoatk.core.ICommand;
import fr.esrf.tangoatk.core.IDevStateScalarListener;
import fr.esrf.tangoatk.core.IDevice;
import fr.esrf.tangoatk.core.attribute.DevStateScalar;
import fr.esrf.tangoatk.core.attribute.StringScalar;
import fr.esrf.tangoatk.core.command.VoidVoidCommand;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.jdraw.SynopticProgressListener;
import fr.esrf.tangoatk.widget.jdraw.TangoSynopticHandler;
import fr.esrf.tangoatk.widget.util.ATKGraphicsUtils;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import fr.esrf.tangoatk.widget.util.ErrorPopup;
import fr.esrf.tangoatk.widget.util.Splash;
import fr.esrf.tangoatk.widget.util.jdraw.JDSwingObject;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;
import javax.swing.JOptionPane;

/**
 *
 * @author pons
 */
public class MainPanel extends javax.swing.JFrame implements ActionListener,SynopticProgressListener,IDevStateScalarListener {
  
  private final static String APP_RELEASE = "3.8";
  public  final String devName;
  public  final String traName;

  static ErrorHistory errWin = null;
  private Splash splash;

  private boolean runningFromShell;
  public static boolean isSR;
  private String oldState="";
  
  private CommandList cmdList;
  private AttributeList attList;
  private DevStateScalar stateModel;

  private ConfigFilePanel configPanel;
  private SeqHistoryPanel seqHistPanel;
  private SYTuningDialog sytuningDialog=null;
  private SRTuningDialog srtuningDialog=null;

  /**
   * Creates new form MainPanel
   */
  public MainPanel(String synopticName) {
    this(synopticName,false);
  }

  public MainPanel(String synopticName,boolean runningFromShell) {
    
    this.runningFromShell = runningFromShell;

    if(!synopticName.startsWith("ssatrans"))
      synopticName = "ssatrans-" + synopticName + ".jdw";

    // Compute transmitter devname
    int i0 = synopticName.indexOf('-');
    int i1 = synopticName.lastIndexOf('.');
    traName = synopticName.substring(i0+1,i1);
    if(traName.toLowerCase().contains("tra0")) {
      devName = "sy/rfssa-tra/" + traName;
      isSR = false;
    } else {
      devName = "srrf/ssa-tra/" + traName;
      isSR = true;
    }

    // Splash window
    splash = new Splash();
    splash.setTitle("RF SSA " + APP_RELEASE);
    splash.setCopyright("(c) ESRF 2012");
    splash.setMaxProgress(100);
    splash.progress(0);

    // Handle windowClosing (Close from the system menu)
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent e) {
        exitForm();
      }
    });

    if( errWin==null ) errWin = new ErrorHistory();
    LoopPanel.errWin = errWin;
    
    initComponents();
    
    cmdList = new CommandList();
    cmdList.addErrorListener(ErrorPopup.getInstance());
    cmdList.addErrorListener(errWin);
    
    attList = new AttributeList();
    attList.addErrorListener(ErrorPopup.getInstance());
    
    // Global commands
    try {
      
      VoidVoidCommand onCmd = (VoidVoidCommand)cmdList.add(devName+"/On");
      onCommandViewer.setModel(onCmd);
      VoidVoidCommand offCmd = (VoidVoidCommand)cmdList.add(devName+"/Off");
      offCommandViewer.setModel(offCmd);
      VoidVoidCommand standbyCmd = (VoidVoidCommand)cmdList.add(devName+"/Standby");
      standbyCommandViewer.setModel(standbyCmd);
      VoidVoidCommand resetCmd = (VoidVoidCommand)cmdList.add(devName+"/Reset");      
      resetCommandViewer.setModel(resetCmd);
      
      stateModel = (DevStateScalar)attList.add(devName+"/State");
      StringScalar statusModel = (StringScalar)attList.add(devName+"/ShortStatus");
      stateStatusViewer.setStateModel(stateModel);
      stateStatusViewer.setStatusModel(statusModel);
      stateModel.addDevStateScalarListener(this);
      
    } catch(ConnectionException e) {      
    }
    
    // Config file panel
    configPanel = new ConfigFilePanel("Transmitter Configuration File",300);
    configPanel.setModel(devName,errWin);
    upPanel.add(configPanel,BorderLayout.CENTER);
    
    // Seq history
    seqHistPanel = new SeqHistoryPanel(this,errWin,devName);
    splitPane.setRightComponent(null);
  
    // Loads the synoptic

    theSynoptic.setProgressListener(this);
    
    InputStream jdFileInStream = this.getClass().getResourceAsStream("/SSATransmitter/"+synopticName);

    if (jdFileInStream == null) {
      splash.setVisible(false);
      JOptionPane.showMessageDialog(
              null, "Failed to get the inputStream for the synoptic file resource : " + synopticName + " \n\n",
              "Resource error",
              JOptionPane.ERROR_MESSAGE);
      exitForm();
    }

    InputStreamReader inStrReader = new InputStreamReader(jdFileInStream);
    try {
      theSynoptic.setErrorHistoryWindow(errWin);
      theSynoptic.setToolTipMode(TangoSynopticHandler.TOOL_TIP_NAME);
      theSynoptic.setAutoZoom(true);
      theSynoptic.loadSynopticFromStream(inStrReader);
    } catch (IOException ioex) {
      splash.setVisible(false);
      JOptionPane.showMessageDialog(
              null, "Cannot find load the synoptic input stream reader.\n" + " Application will abort ...\n" + ioex,
              "Resource access failed",
              JOptionPane.ERROR_MESSAGE);
      exitForm();
    }

    initTransistorAlarms();
    
    attList.setRefreshInterval(1000);
    attList.startRefresher();

    splash.setVisible(false);
    setTitle("SSA Transmitter " + APP_RELEASE + " [" + traName + "]");
    ATKGraphicsUtils.centerFrameOnScreen(this);
    setVisible(true);
    
  }
  
  private void clearModel() {

    theSynoptic.clearSynopticFileModel();
    if(stateModel!=null) stateModel.removeDevStateScalarListener(this);
    offCommandViewer.setModel((ICommand)null);
    standbyCommandViewer.setModel((ICommand)null);
    onCommandViewer.setModel((ICommand)null);
    resetCommandViewer.setModel((ICommand)null);    
    cmdList.removeErrorListener(errWin);
    cmdList.clear();
    attList.removeErrorListener(errWin);
    stateStatusViewer.setStateModel(null);
    stateStatusViewer.setStatusModel(null);
    attList.stopRefresher();
    attList.clear();
    configPanel.clearModel();
    seqHistPanel.clearModel();
    if( sytuningDialog!=null ) sytuningDialog.clearModel();
    if( srtuningDialog!=null ) srtuningDialog.clearModel();

  }
  
  private void initTransistorAlarms() {

    final String[] names;
    final String twName;
    final String twAlarmName;

    if( isSR ) {
      // SR
      names = new String[]{ traName+"M",traName+"S" };
      twName = "srrf/ssa-tower/";
      twAlarmName = "srrf/ssa-alarm/";
    } else {
      // SYRF
      names = new String[]{ traName+"M",traName+"S" };
      twName = "sy/rfssa-tower/";
      twAlarmName = "sy/rfssa-alarm/";
    }

      // SYRF
    for (int i = 0; i < names.length; i++) {

      Vector v;
      SimpleScalarViewer sv;
      JDSwingObject obj;
      final int idx = i;
      final MouseAdapter mA = new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          TransistorAlarmDialog dlg = new TransistorAlarmDialog(errWin,isSR,names[idx]);
        }
      };

      v = theSynoptic.getObjectsByName(twAlarmName + names[i] + "/DisabledTransistorCount", false);
      obj = (JDSwingObject) v.get(0);
      sv = (SimpleScalarViewer) obj.getComponent();
      sv.setEditable(false);
      sv.addMouseListener(mA);

      v = theSynoptic.getObjectsByName(twName + names[i] + "/TransistorFailures", false);
      obj = (JDSwingObject) v.get(0);
      sv = (SimpleScalarViewer) obj.getComponent();
      sv.setEditable(false);
      sv.addMouseListener(mA);

    }

  }

  public void progress(double p) {
    splash.progress((int)(p*100.0));
  }
  
  public void showSeqHistory() {

    splitPane.setRightComponent(seqHistPanel);
    pack();

  }

  public void hideSeqHistory() {

    splitPane.remove(seqHistPanel);
    pack();

  }
  
  @Override
  public void devStateScalarChange(DevStateScalarEvent dsse) {

    String state = dsse.getValue();
    
    if(  state.equalsIgnoreCase(IDevice.MOVING) &&
        !state.equalsIgnoreCase(oldState) )
      showSeqHistory();

    oldState = state;
    
  }

  @Override
  public void stateChange(AttributeStateEvent ase) {
  }

  @Override
  public void errorChange(ErrorEvent ee) {
  }


  /**
   * Exit the application
   */
  public void exitForm() {

    if (runningFromShell) {

      System.exit(0);

    } else {

      clearModel();
      setVisible(false);
      dispose();

    }

  }

  /**
   * This method is called from within the constructor to initialize the form.
   * WARNING: Do NOT modify this code. The content of this method is always
   * regenerated by the Form Editor.
   */
  @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {
    java.awt.GridBagConstraints gridBagConstraints;

    splitPane = new javax.swing.JSplitPane();
    innerPanel = new javax.swing.JPanel();
    upPanel = new javax.swing.JPanel();
    centerPanel = new javax.swing.JPanel();
    theSynoptic = new fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer();
    downPanel = new javax.swing.JPanel();
    stateStatusViewer = new fr.esrf.tangoatk.widget.attribute.StateStatusViewer();
    offCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
    standbyCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
    onCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
    resetCommandViewer = new fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer();
    menuBar = new javax.swing.JMenuBar();
    fileMenu = new javax.swing.JMenu();
    exitMenuItem = new javax.swing.JMenuItem();
    viewMenu = new javax.swing.JMenu();
    tuningMenuItem = new javax.swing.JMenuItem();
    sequenceMenuItem = new javax.swing.JMenuItem();
    jSeparator1 = new javax.swing.JPopupMenu.Separator();
    diagMenuItem = new javax.swing.JMenuItem();
    errorMenuItem = new javax.swing.JMenuItem();

    setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

    splitPane.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

    innerPanel.setLayout(new java.awt.BorderLayout());

    upPanel.setLayout(new java.awt.BorderLayout());
    innerPanel.add(upPanel, java.awt.BorderLayout.NORTH);

    centerPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
    centerPanel.setLayout(new java.awt.BorderLayout());
    centerPanel.add(theSynoptic, java.awt.BorderLayout.CENTER);

    innerPanel.add(centerPanel, java.awt.BorderLayout.CENTER);

    downPanel.setLayout(new java.awt.GridBagLayout());
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 0;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.weightx = 1.0;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 0);
    downPanel.add(stateStatusViewer, gridBagConstraints);

    offCommandViewer.setText("Off");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 1;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 0);
    downPanel.add(offCommandViewer, gridBagConstraints);

    standbyCommandViewer.setText("Standby");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 2;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 0);
    downPanel.add(standbyCommandViewer, gridBagConstraints);

    onCommandViewer.setText("On");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 3;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 0);
    downPanel.add(onCommandViewer, gridBagConstraints);

    resetCommandViewer.setText("Reset");
    gridBagConstraints = new java.awt.GridBagConstraints();
    gridBagConstraints.gridx = 4;
    gridBagConstraints.gridy = 0;
    gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
    gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
    gridBagConstraints.insets = new java.awt.Insets(3, 5, 3, 5);
    downPanel.add(resetCommandViewer, gridBagConstraints);

    innerPanel.add(downPanel, java.awt.BorderLayout.SOUTH);

    splitPane.setLeftComponent(innerPanel);

    getContentPane().add(splitPane, java.awt.BorderLayout.CENTER);

    fileMenu.setText("File");

    exitMenuItem.setText("Exit");
    exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        exitMenuItemActionPerformed(evt);
      }
    });
    fileMenu.add(exitMenuItem);

    menuBar.add(fileMenu);

    viewMenu.setText("View");

    tuningMenuItem.setText("Tuning Panel...");
    tuningMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        tuningMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(tuningMenuItem);

    sequenceMenuItem.setText("Sequence History");
    sequenceMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        sequenceMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(sequenceMenuItem);
    viewMenu.add(jSeparator1);

    diagMenuItem.setText("Diagnostics...");
    diagMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        diagMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(diagMenuItem);

    errorMenuItem.setText("Errors...");
    errorMenuItem.addActionListener(new java.awt.event.ActionListener() {
      public void actionPerformed(java.awt.event.ActionEvent evt) {
        errorMenuItemActionPerformed(evt);
      }
    });
    viewMenu.add(errorMenuItem);

    menuBar.add(viewMenu);

    setJMenuBar(menuBar);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
      exitForm();
  }//GEN-LAST:event_exitMenuItemActionPerformed

  private void tuningMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tuningMenuItemActionPerformed
      if( isSR ) {
        if( srtuningDialog==null ) srtuningDialog = new SRTuningDialog(errWin,traName);
        ATKGraphicsUtils.centerFrameOnScreen(srtuningDialog);
        srtuningDialog.setVisible(true);
      } else {
        if( sytuningDialog==null ) sytuningDialog = new SYTuningDialog(errWin,traName);
        ATKGraphicsUtils.centerFrameOnScreen(sytuningDialog);
        sytuningDialog.setVisible(true);
      }
  }//GEN-LAST:event_tuningMenuItemActionPerformed

  private void sequenceMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sequenceMenuItemActionPerformed
    showSeqHistory();
  }//GEN-LAST:event_sequenceMenuItemActionPerformed

  private void diagMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_diagMenuItemActionPerformed
    fr.esrf.tangoatk.widget.util.ATKDiagnostic.showDiagnostic();
  }//GEN-LAST:event_diagMenuItemActionPerformed

  private void errorMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_errorMenuItemActionPerformed
    ATKGraphicsUtils.centerFrameOnScreen(errWin);
    errWin.setVisible(true);
  }//GEN-LAST:event_errorMenuItemActionPerformed

  /**
   * @param args the command line arguments
   */
  public static void main(String args[]) {
    
    if( args.length!=1 ) {
      System.out.println("Usage: jssatrans transmitter_synoptic_name");
      System.exit(0);
    }
    new MainPanel(args[0],true);
    
  }

  // Variables declaration - do not modify//GEN-BEGIN:variables
  private javax.swing.JPanel centerPanel;
  private javax.swing.JMenuItem diagMenuItem;
  private javax.swing.JPanel downPanel;
  private javax.swing.JMenuItem errorMenuItem;
  private javax.swing.JMenuItem exitMenuItem;
  private javax.swing.JMenu fileMenu;
  private javax.swing.JPanel innerPanel;
  private javax.swing.JPopupMenu.Separator jSeparator1;
  private javax.swing.JMenuBar menuBar;
  private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer offCommandViewer;
  private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer onCommandViewer;
  private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer resetCommandViewer;
  private javax.swing.JMenuItem sequenceMenuItem;
  private javax.swing.JSplitPane splitPane;
  private fr.esrf.tangoatk.widget.command.VoidVoidCommandViewer standbyCommandViewer;
  private fr.esrf.tangoatk.widget.attribute.StateStatusViewer stateStatusViewer;
  private fr.esrf.tangoatk.widget.jdraw.SynopticFileViewer theSynoptic;
  private javax.swing.JMenuItem tuningMenuItem;
  private javax.swing.JPanel upPanel;
  private javax.swing.JMenu viewMenu;
  // End of variables declaration//GEN-END:variables

    @Override
    public void actionPerformed(ActionEvent e) {
        if( e.getSource()==seqHistPanel ) {
            if( e.getActionCommand()=="ShowSeqHistory") {
              showSeqHistory();
            } else if ( e.getActionCommand()=="HideSeqHistory") {
              hideSeqHistory();                
            }
        }
    }
  
}
