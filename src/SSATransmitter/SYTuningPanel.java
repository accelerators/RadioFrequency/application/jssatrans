/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SSATransmitter;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.attribute.BooleanScalar;
import fr.esrf.tangoatk.core.attribute.NumberScalar;
import fr.esrf.tangoatk.widget.attribute.BooleanScalarComboEditor;
import fr.esrf.tangoatk.widget.attribute.NumberScalarWheelEditor;
import fr.esrf.tangoatk.widget.attribute.SimpleScalarViewer;
import fr.esrf.tangoatk.widget.properties.LabelViewer;
import fr.esrf.tangoatk.widget.util.ErrorHistory;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

/**
 *
 * @author pons
 */
public class SYTuningPanel extends JPanel {
  
  private final static Font borderFont = new Font("Dialog",Font.BOLD,12);
  private final static Font viewerFont = new Font("Dialog",Font.BOLD,14);
  private final static Font setterFont = new Font("Lucida Bright", Font.PLAIN, 14);

  private String traName;
  private String traIdx;
  private String cavName;

  private AttributeList attList;

  private LabelViewer              traPinAttLabel;
  private SimpleScalarViewer       traPinAttViewer;
  private NumberScalarWheelEditor  traPinAttSetter;
  private LabelViewer              traAmpRFLowLabel;
  private SimpleScalarViewer       traAmpRFLowViewer;
  private NumberScalarWheelEditor  traAmpRFLowSetter;
  private LabelViewer              traPhaseRFLowLabel;
  private SimpleScalarViewer       traPhaseRFLowViewer;
  private NumberScalarWheelEditor  traPhaseRFLowSetter;
  private LabelViewer              traRegulationONLabel;
  private BooleanScalarComboEditor traRegulationONSetter;
  private LabelViewer              traDistribPinAttLabel;
  private SimpleScalarViewer       traDistribPinAttViewer;
  private NumberScalarWheelEditor  traDistribPinAttSetter;
  private LabelViewer              traPhaseAdjustLabel;
  private SimpleScalarViewer       traPhaseAdjustViewer;
  private NumberScalarWheelEditor  traPhaseAdjustSetter;

  private LabelViewer              phaAmpLabel;
  private SimpleScalarViewer       phaAmpViewer;
  private NumberScalarWheelEditor  phaAmpSetter;
  private LabelViewer              phaAmpExtLabel;
  private SimpleScalarViewer       phaAmpExtViewer;
  private NumberScalarWheelEditor  phaAmpExtSetter;
  private LabelViewer              phaPhaseLabel;
  private SimpleScalarViewer       phaPhaseViewer;
  private NumberScalarWheelEditor  phaPhaseSetter;
  private LabelViewer              phaGainMagLabel;
  private SimpleScalarViewer       phaGainMagViewer;
  private NumberScalarWheelEditor  phaGainMagSetter;
  private LabelViewer              phaGainPhaLabel;
  private SimpleScalarViewer       phaGainPhaViewer;
  private NumberScalarWheelEditor  phaGainPhaSetter;
  private LabelViewer              phaT0Label;
  private SimpleScalarViewer       phaT0Viewer;
  private NumberScalarWheelEditor  phaT0Setter;
  private LabelViewer              phaTriggerLabel;
  private BooleanScalarComboEditor phaTriggerSetter;

  private LabelViewer              phaOffsetLabel;
  private SimpleScalarViewer       phaOffsetViewer;
  private NumberScalarWheelEditor  phaOffsetSetter;
  private LabelViewer              initPositionLabel;
  private SimpleScalarViewer       initPositionViewer;
  private NumberScalarWheelEditor  initPositionSetter;
  private LabelViewer              initPosition2Label;
  private SimpleScalarViewer       initPosition2Viewer;
  private NumberScalarWheelEditor  initPosition2Setter;

  private JButton dismissBtn;
  private ErrorHistory errWin;

  public SYTuningPanel(String traName,ErrorHistory errWin) {

    
    this.errWin = errWin;        
    this.traName = traName;
    traIdx = traName.substring(traName.length()-1);
    cavName = "cav" + traIdx;

    attList = new AttributeList();
    attList.addErrorListener(errWin);

    setLayout(null);
    setPreferredSize(new Dimension(380,715));

    // Transmitter ------------------------------------------------------------------------------------

    JPanel traPanel = new JPanel();
    traPanel.setLayout(null);
    traPanel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Transmitter",
        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
        borderFont, Color.BLACK) );
    traPanel.setBounds(5,5,370,260);
    add(traPanel);

    traPinAttLabel = new LabelViewer();
    traPinAttLabel.setBackground(getBackground());
    traPinAttLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traPinAttLabel.setBounds(10,20,140,30);
    traPanel.add(traPinAttLabel);
    traPinAttViewer = new SimpleScalarViewer();
    traPinAttViewer.setFont(viewerFont);
    traPinAttViewer.setBackground(getBackground());
    traPinAttViewer.setAlarmEnabled(false);
    traPinAttViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traPinAttViewer.setBounds(150,18,100,35);
    traPanel.add(traPinAttViewer);
    traPinAttSetter = new NumberScalarWheelEditor();
    traPinAttSetter.setBackground(getBackground());
    traPinAttSetter.setFont(setterFont);
    traPinAttSetter.setBounds(250,15,115,40);
    traPanel.add(traPinAttSetter);
    traAmpRFLowLabel = new LabelViewer();
    traAmpRFLowLabel.setBackground(getBackground());
    traAmpRFLowLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traAmpRFLowLabel.setBounds(10,60,140,30);
    traPanel.add(traAmpRFLowLabel);
    traAmpRFLowViewer = new SimpleScalarViewer();
    traAmpRFLowViewer.setFont(viewerFont);
    traAmpRFLowViewer.setBackground(getBackground());
    traAmpRFLowViewer.setAlarmEnabled(false);
    traAmpRFLowViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traAmpRFLowViewer.setBounds(150,58,100,35);
    traPanel.add(traAmpRFLowViewer);
    traAmpRFLowSetter = new NumberScalarWheelEditor();
    traAmpRFLowSetter.setBackground(getBackground());
    traAmpRFLowSetter.setFont(setterFont);
    traAmpRFLowSetter.setBounds(250,55,115,40);
    traPanel.add(traAmpRFLowSetter);
    traPhaseRFLowLabel = new LabelViewer();
    traPhaseRFLowLabel.setBackground(getBackground());
    traPhaseRFLowLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traPhaseRFLowLabel.setBounds(10,100,140,30);
    traPanel.add(traPhaseRFLowLabel);
    traPhaseRFLowViewer = new SimpleScalarViewer();
    traPhaseRFLowViewer.setFont(viewerFont);
    traPhaseRFLowViewer.setBackground(getBackground());
    traPhaseRFLowViewer.setAlarmEnabled(false);
    traPhaseRFLowViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traPhaseRFLowViewer.setBounds(150,98,100,35);
    traPanel.add(traPhaseRFLowViewer);
    traPhaseRFLowSetter = new NumberScalarWheelEditor();
    traPhaseRFLowSetter.setBackground(getBackground());
    traPhaseRFLowSetter.setFont(setterFont);
    traPhaseRFLowSetter.setBounds(250,95,115,40);
    traPanel.add(traPhaseRFLowSetter);

    traRegulationONLabel = new LabelViewer();
    traRegulationONLabel.setBackground(getBackground());
    traRegulationONLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traRegulationONLabel.setBounds(10,140,140,30);
    traPanel.add(traRegulationONLabel);
    traRegulationONSetter = new BooleanScalarComboEditor();
    traRegulationONSetter.setBackground(getBackground());
    traRegulationONSetter.setBounds(150,140,115,30);
    traPanel.add(traRegulationONSetter);

    traDistribPinAttLabel = new LabelViewer();
    traDistribPinAttLabel.setBackground(getBackground());
    traDistribPinAttLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traDistribPinAttLabel.setBounds(10,180,140,30);
    traPanel.add(traDistribPinAttLabel);
    traDistribPinAttViewer = new SimpleScalarViewer();
    traDistribPinAttViewer.setFont(viewerFont);
    traDistribPinAttViewer.setBackground(getBackground());
    traDistribPinAttViewer.setAlarmEnabled(false);
    traDistribPinAttViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traDistribPinAttViewer.setBounds(150,178,100,35);
    traPanel.add(traDistribPinAttViewer);
    traDistribPinAttSetter = new NumberScalarWheelEditor();
    traDistribPinAttSetter.setBackground(getBackground());
    traDistribPinAttSetter.setFont(setterFont);
    traDistribPinAttSetter.setBounds(250,175,115,40);
    traPanel.add(traDistribPinAttSetter);

    traPhaseAdjustLabel = new LabelViewer();
    traPhaseAdjustLabel.setBackground(getBackground());
    traPhaseAdjustLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traPhaseAdjustLabel.setBounds(10,220,140,30);
    traPanel.add(traPhaseAdjustLabel);
    traPhaseAdjustViewer = new SimpleScalarViewer();
    traPhaseAdjustViewer.setFont(viewerFont);
    traPhaseAdjustViewer.setBackground(getBackground());
    traPhaseAdjustViewer.setAlarmEnabled(false);
    traPhaseAdjustViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traPhaseAdjustViewer.setBounds(150,218,100,35);
    traPanel.add(traPhaseAdjustViewer);
    traPhaseAdjustSetter = new NumberScalarWheelEditor();
    traPhaseAdjustSetter.setBackground(getBackground());
    traPhaseAdjustSetter.setFont(setterFont);
    traPhaseAdjustSetter.setBounds(250,215,115,40);
    traPanel.add(traPhaseAdjustSetter);

    // driver loops ------------------------------------------------------------------------------------

    JPanel phaPanel = new JPanel();
    phaPanel.setLayout(null);
    phaPanel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Driver regulation Loops",
        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
        borderFont, Color.BLACK) );
    phaPanel.setBounds(5,265,370,300);
    add(phaPanel);

    phaAmpLabel = new LabelViewer();
    phaAmpLabel.setBackground(getBackground());
    phaAmpLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaAmpLabel.setBounds(10,20,140,30);
    phaPanel.add(phaAmpLabel);
    phaAmpViewer = new SimpleScalarViewer();
    phaAmpViewer.setFont(viewerFont);
    phaAmpViewer.setBackground(getBackground());
    phaAmpViewer.setAlarmEnabled(false);
    phaAmpViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaAmpViewer.setBounds(150,18,100,35);
    phaPanel.add(phaAmpViewer);
    phaAmpSetter = new NumberScalarWheelEditor();
    phaAmpSetter.setBackground(getBackground());
    phaAmpSetter.setFont(setterFont);
    phaAmpSetter.setBounds(250,15,115,40);
    phaPanel.add(phaAmpSetter);
    
    phaAmpExtLabel = new LabelViewer();
    phaAmpExtLabel.setBackground(getBackground());
    phaAmpExtLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaAmpExtLabel.setBounds(10,60,140,30);
    phaPanel.add(phaAmpExtLabel);
    phaAmpExtViewer = new SimpleScalarViewer();
    phaAmpExtViewer.setFont(viewerFont);
    phaAmpExtViewer.setBackground(getBackground());
    phaAmpExtViewer.setAlarmEnabled(false);
    phaAmpExtViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaAmpExtViewer.setBounds(150,58,100,35);
    phaPanel.add(phaAmpExtViewer);
    phaAmpExtSetter = new NumberScalarWheelEditor();
    phaAmpExtSetter.setBackground(getBackground());
    phaAmpExtSetter.setFont(setterFont);
    phaAmpExtSetter.setBounds(250,55,115,40);
    phaPanel.add(phaAmpExtSetter);
    
    phaPhaseLabel = new LabelViewer();
    phaPhaseLabel.setBackground(getBackground());
    phaPhaseLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaPhaseLabel.setBounds(10,100,140,30);
    phaPanel.add(phaPhaseLabel);
    phaPhaseViewer = new SimpleScalarViewer();
    phaPhaseViewer.setFont(viewerFont);
    phaPhaseViewer.setBackground(getBackground());
    phaPhaseViewer.setAlarmEnabled(false);
    phaPhaseViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaPhaseViewer.setBounds(150,98,100,35);
    phaPanel.add(phaPhaseViewer);
    phaPhaseSetter = new NumberScalarWheelEditor();
    phaPhaseSetter.setBackground(getBackground());
    phaPhaseSetter.setFont(setterFont);
    phaPhaseSetter.setBounds(250,95,115,40);
    phaPanel.add(phaPhaseSetter);
    
    phaGainMagLabel = new LabelViewer();
    phaGainMagLabel.setBackground(getBackground());
    phaGainMagLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaGainMagLabel.setBounds(10, 140, 140, 30);
    phaPanel.add(phaGainMagLabel);
    phaGainMagViewer = new SimpleScalarViewer();
    phaGainMagViewer.setFont(viewerFont);
    phaGainMagViewer.setBackground(getBackground());
    phaGainMagViewer.setAlarmEnabled(false);
    phaGainMagViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaGainMagViewer.setBounds(150, 138, 100, 35);
    phaPanel.add(phaGainMagViewer);
    phaGainMagSetter = new NumberScalarWheelEditor();
    phaGainMagSetter.setBackground(getBackground());
    phaGainMagSetter.setFont(setterFont);
    phaGainMagSetter.setBounds(250, 135, 115, 40);
    phaPanel.add(phaGainMagSetter);
    
    phaGainPhaLabel = new LabelViewer();
    phaGainPhaLabel.setBackground(getBackground());
    phaGainPhaLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaGainPhaLabel.setBounds(10, 180, 140, 30);
    phaPanel.add(phaGainPhaLabel);
    phaGainPhaViewer = new SimpleScalarViewer();
    phaGainPhaViewer.setFont(viewerFont);
    phaGainPhaViewer.setBackground(getBackground());
    phaGainPhaViewer.setAlarmEnabled(false);
    phaGainPhaViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaGainPhaViewer.setBounds(150, 178, 100, 35);
    phaPanel.add(phaGainPhaViewer);
    phaGainPhaSetter = new NumberScalarWheelEditor();
    phaGainPhaSetter.setBackground(getBackground());
    phaGainPhaSetter.setFont(setterFont);
    phaGainPhaSetter.setBounds(250, 175, 115, 40);
    phaPanel.add(phaGainPhaSetter);
    
    phaT0Label = new LabelViewer();
    phaT0Label.setBackground(getBackground());
    phaT0Label.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaT0Label.setBounds(10, 220, 140, 30);
    phaPanel.add(phaT0Label);
    phaT0Viewer = new SimpleScalarViewer();
    phaT0Viewer.setFont(viewerFont);
    phaT0Viewer.setBackground(getBackground());
    phaT0Viewer.setAlarmEnabled(false);
    phaT0Viewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaT0Viewer.setBounds(150, 218, 100, 35);
    phaPanel.add(phaT0Viewer);
    phaT0Setter = new NumberScalarWheelEditor();
    phaT0Setter.setBackground(getBackground());
    phaT0Setter.setFont(setterFont);
    phaT0Setter.setBounds(250, 215, 115, 40);
    phaPanel.add(phaT0Setter);

    phaTriggerLabel = new LabelViewer();
    phaTriggerLabel.setBackground(getBackground());
    phaTriggerLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaTriggerLabel.setBounds(10,260,140,30);
    phaPanel.add(phaTriggerLabel);
    phaTriggerSetter = new BooleanScalarComboEditor();
    phaTriggerSetter.setBackground(getBackground());
    phaTriggerSetter.setFont(viewerFont);
    phaTriggerSetter.setBounds(150,255,115,30);
    phaPanel.add(phaTriggerSetter);

    // tuner loops ------------------------------------------------------------------------------------

    JPanel tunPanel = new JPanel();
    tunPanel.setLayout(null);
    tunPanel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Tuner regulation Loop",
        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
        borderFont, Color.BLACK) );
    tunPanel.setBounds(5,570,370,140);
    add(tunPanel);

    phaOffsetLabel = new LabelViewer();
    phaOffsetLabel.setBackground(getBackground());
    phaOffsetLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaOffsetLabel.setBounds(10,20,140,30);
    tunPanel.add(phaOffsetLabel);
    phaOffsetViewer = new SimpleScalarViewer();
    phaOffsetViewer.setFont(viewerFont);
    phaOffsetViewer.setBackground(getBackground());
    phaOffsetViewer.setAlarmEnabled(false);
    phaOffsetViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaOffsetViewer.setBounds(150,18,100,35);
    tunPanel.add(phaOffsetViewer);
    phaOffsetSetter = new NumberScalarWheelEditor();
    phaOffsetSetter.setBackground(getBackground());
    phaOffsetSetter.setFont(setterFont);
    phaOffsetSetter.setBounds(250,15,115,40);
    tunPanel.add(phaOffsetSetter);

    initPositionLabel = new LabelViewer();
    initPositionLabel.setBackground(getBackground());
    initPositionLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    initPositionLabel.setBounds(10,60,140,30);
    tunPanel.add(initPositionLabel);
    initPositionViewer = new SimpleScalarViewer();
    initPositionViewer.setFont(viewerFont);
    initPositionViewer.setBackground(getBackground());
    initPositionViewer.setAlarmEnabled(false);
    initPositionViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    initPositionViewer.setBounds(150,58,100,35);
    tunPanel.add(initPositionViewer);
    initPositionSetter = new NumberScalarWheelEditor();
    initPositionSetter.setBackground(getBackground());
    initPositionSetter.setFont(setterFont);
    initPositionSetter.setBounds(250,55,115,40);
    tunPanel.add(initPositionSetter);

    initPosition2Label = new LabelViewer();
    initPosition2Label.setBackground(getBackground());
    initPosition2Label.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    initPosition2Label.setBounds(10,100,140,30);
    tunPanel.add(initPosition2Label);
    initPosition2Viewer = new SimpleScalarViewer();
    initPosition2Viewer.setFont(viewerFont);
    initPosition2Viewer.setBackground(getBackground());
    initPosition2Viewer.setAlarmEnabled(false);
    initPosition2Viewer.setBorder(BorderFactory.createLoweredBevelBorder());
    initPosition2Viewer.setBounds(150,98,100,35);
    tunPanel.add(initPosition2Viewer);
    initPosition2Setter = new NumberScalarWheelEditor();
    initPosition2Setter.setBackground(getBackground());
    initPosition2Setter.setFont(setterFont);
    initPosition2Setter.setBounds(250,95,115,40);
    tunPanel.add(initPosition2Setter);

    try {

      NumberScalar traPinAtt = (NumberScalar)attList.add("sy/rfssa-tra/" + traName +"/PinAtt_Nominal");
      traPinAttLabel.setModel(traPinAtt);
      traPinAttViewer.setModel(traPinAtt);
      traPinAttSetter.setModel(traPinAtt);

      NumberScalar traDistribPinAtt = (NumberScalar)attList.add("sy/rfssa-pinatt/tra0/Attenuation" + traIdx);
      traDistribPinAttLabel.setModel(traDistribPinAtt);
      traDistribPinAttViewer.setModel(traDistribPinAtt);
      traDistribPinAttSetter.setModel(traDistribPinAtt);

      NumberScalar traPhaseAdjust = (NumberScalar)attList.add("sy/rfssa-tra/phase/Phase_SSA_" + traIdx);
      traPhaseAdjustLabel.setModel(traPhaseAdjust);
      traPhaseAdjustViewer.setModel(traPhaseAdjust);
      traPhaseAdjustSetter.setModel(traPhaseAdjust);

      NumberScalar traAmpRFLow = (NumberScalar)attList.add("sy/rfssa-tra/" + traName +"/Amplitude_RFLow");
      traAmpRFLowLabel.setModel(traAmpRFLow);
      traAmpRFLowViewer.setModel(traAmpRFLow);
      traAmpRFLowSetter.setModel(traAmpRFLow);

      NumberScalar traPhaseRFLow = (NumberScalar)attList.add("sy/rfssa-tra/" + traName +"/Phase_RFLow");
      traPhaseRFLowLabel.setModel(traPhaseRFLow);
      traPhaseRFLowViewer.setModel(traPhaseRFLow);
      traPhaseRFLowSetter.setModel(traPhaseRFLow);

      BooleanScalar regulation_ON = (BooleanScalar)attList.add("sy/rfssa-tra/" + traName +"/Regulation_ON");
      traRegulationONLabel.setModel(regulation_ON);
      traRegulationONSetter.setAttModel(regulation_ON);

      NumberScalar phaAmp = (NumberScalar)attList.add("sy/rfssa-driver/" + traName +"/RFAmplitude");
      phaAmpLabel.setModel(phaAmp);
      NumberScalar phaAmpV = (NumberScalar)attList.add("sy/rfssa-driver/" + traName + "/CavInjVoltage");
      phaAmpViewer.setModel(phaAmpV);
      phaAmpSetter.setModel(phaAmp);
      
      NumberScalar phaAmpExt = (NumberScalar)attList.add("sy/rfssa-driver/" + traName +"/RFWaveformAmplitude");
      phaAmpExtLabel.setModel(phaAmpExt);
      NumberScalar phaAmpExtV = (NumberScalar)attList.add("sy/rfssa-driver/" + traName + "/CavVoltage");
      phaAmpExtViewer.setModel(phaAmpExtV);
      phaAmpExtSetter.setModel(phaAmpExt);

      NumberScalar phaPha = (NumberScalar)attList.add("sy/rfssa-driver/" + traName +"/RFPhase");
      phaPhaseLabel.setModel(phaPha);
      NumberScalar phaPhaP = (NumberScalar)attList.add("sy/rfcav-adc/" + cavName + "/Phase_Cavity_1");
      phaPhaseViewer.setModel(phaPhaP);
      phaPhaseSetter.setModel(phaPha);

      NumberScalar phaGainI = (NumberScalar)attList.add("sy/rfssa-driver/" + traName +"/MagLoopGain");
      phaGainPhaLabel.setModel(phaGainI);
      phaGainPhaViewer.setModel(phaGainI);
      phaGainPhaSetter.setModel(phaGainI);

      NumberScalar phaGainP = (NumberScalar)attList.add("sy/rfssa-driver/" + traName +"/PhaseLoopGain");
      phaGainMagLabel.setModel(phaGainP);
      phaGainMagViewer.setModel(phaGainP);
      phaGainMagSetter.setModel(phaGainP);

      NumberScalar phaT0 = (NumberScalar)attList.add("sy/rfssa-driver/" + traName +"/T0Delay");
      phaT0Label.setModel(phaT0);
      phaT0Viewer.setModel(phaT0);
      phaT0Setter.setModel(phaT0);

      BooleanScalar phaTrigger = (BooleanScalar)attList.add("sy/rfssa-driver/" + traName +"/TriggerMode");
      phaTriggerLabel.setModel(phaTrigger);
      phaTriggerSetter.setAttModel(phaTrigger);

      NumberScalar phaOffset = (NumberScalar)attList.add("sy/rfcav-tunerloop/" + cavName +"/TuningAngle");
      phaOffsetLabel.setModel(phaOffset);
      phaOffsetViewer.setModel(phaOffset);
      phaOffsetSetter.setModel(phaOffset);

      NumberScalar initPos = (NumberScalar)attList.add("sy/rfcav-tunerloop/" + cavName +"/InitMotorPosition");
      initPositionLabel.setModel(initPos);
      initPositionViewer.setModel(initPos);
      initPositionSetter.setModel(initPos);

      NumberScalar initPos2 = (NumberScalar)attList.add("sy/rfcav-tunerloop/" + cavName +"/InitMotorPosition2");
      initPosition2Label.setModel(initPos2);
      initPosition2Viewer.setModel(initPos2);
      initPosition2Setter.setModel(initPos2);

    } catch(ConnectionException e) {
      System.out.println(e.getMessage());
    }

    attList.startRefresher();
    
  }

  public void clearModel() {

    attList.removeErrorListener(errWin);
    attList.stopRefresher();

    traPinAttLabel.setModel(null);
    traPinAttViewer.clearModel();
    traPinAttSetter.setModel(null);
    traDistribPinAttLabel.setModel(null);
    traDistribPinAttViewer.clearModel();
    traDistribPinAttSetter.setModel(null);
    traAmpRFLowLabel.setModel(null);
    traAmpRFLowViewer.clearModel();
    traAmpRFLowSetter.setModel(null);
    traPhaseRFLowLabel.setModel(null);
    traPhaseRFLowViewer.clearModel();
    traPhaseRFLowSetter.setModel(null);
    traRegulationONLabel.setModel(null);
    traRegulationONSetter.setAttModel(null);

    phaAmpLabel.setModel(null);
    phaAmpViewer.clearModel();
    phaAmpSetter.setModel(null);
    phaAmpExtLabel.setModel(null);
    phaAmpExtViewer.clearModel();
    phaAmpExtSetter.setModel(null);
    phaPhaseLabel.setModel(null);
    phaPhaseViewer.clearModel();
    phaPhaseSetter.setModel(null);
    phaGainPhaLabel.setModel(null);
    phaGainPhaViewer.clearModel();
    phaGainPhaSetter.setModel(null);
    phaGainMagLabel.setModel(null);
    phaGainMagViewer.clearModel();
    phaGainMagSetter.setModel(null);
    phaT0Label.setModel(null);
    phaT0Viewer.clearModel();
    phaT0Setter.setModel(null);
    phaTriggerLabel.setModel(null);
    phaTriggerSetter.setAttModel(null);

    phaOffsetLabel.setModel(null);
    phaOffsetViewer.clearModel();
    phaOffsetSetter.setModel(null);

    initPositionLabel.setModel(null);
    initPositionViewer.clearModel();
    initPositionSetter.setModel(null);
    initPosition2Label.setModel(null);
    initPosition2Viewer.clearModel();
    initPosition2Setter.setModel(null);

  }

}
