package SSATransmitter;

import fr.esrf.tangoatk.widget.util.ErrorHistory;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Tuning panels.
 */
public class SYTuningDialog extends JFrame implements ActionListener {

  private JButton dismissBtn;
  private SYTuningPanel syPanel;

  public SYTuningDialog(ErrorHistory errWin,String traName) {

    syPanel = new SYTuningPanel(traName,errWin);

    JPanel globalPanel = new JPanel();
    globalPanel.setLayout(new BorderLayout());

    JScrollPane scrollPane = new JScrollPane(syPanel);
    globalPanel.add(scrollPane,BorderLayout.CENTER);

    JPanel btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    globalPanel.add(btnPanel,BorderLayout.SOUTH);
    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    btnPanel.add(dismissBtn);


    setContentPane(globalPanel);
    setTitle("Tuning panel (" + traName.toUpperCase() + ")");

  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if( src==dismissBtn ) {
      setVisible(false);
    }

  }

  public void clearModel() {    
    syPanel.clearModel();
  }

}
