package SSATransmitter;

import fr.esrf.tangoatk.core.AttributeList;
import fr.esrf.tangoatk.core.ConnectionException;
import fr.esrf.tangoatk.core.attribute.BooleanScalar;
import fr.esrf.tangoatk.core.attribute.NumberScalar;
import fr.esrf.tangoatk.widget.attribute.*;
import fr.esrf.tangoatk.widget.properties.LabelViewer;
import fr.esrf.tangoatk.widget.util.ErrorHistory;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Tuning panels.
 */
public class SRTuningDialog extends JFrame implements ActionListener {

  private final static Font borderFont = new Font("Dialog",Font.BOLD,12);
  private final static Font viewerFont = new Font("Dialog",Font.BOLD,14);
  private final static Font setterFont = new Font("Lucida Bright", Font.PLAIN, 14);

  private String traName;
  private String cavName;
  private String traIdx;

  private AttributeList attList;

  private LabelViewer              traPinAttLabel;
  private SimpleScalarViewer       traPinAttViewer;
  private NumberScalarWheelEditor  traPinAttSetter;
  private LabelViewer              traAmpRFLowLabel;
  private SimpleScalarViewer       traAmpRFLowViewer;
  private NumberScalarWheelEditor  traAmpRFLowSetter;
  private LabelViewer              traPhaseRFLowLabel;
  private SimpleScalarViewer       traPhaseRFLowViewer;
  private NumberScalarWheelEditor  traPhaseRFLowSetter;
  private LabelViewer              traRegulationONLabel;
  private BooleanScalarComboEditor traRegulationONSetter;
  private LabelViewer              traDistribPinAttLabel;
  private SimpleScalarViewer       traDistribPinAttViewer;
  private NumberScalarWheelEditor  traDistribPinAttSetter;
  private LabelViewer              traPhaseAdjustLabel;
  private SimpleScalarViewer       traPhaseAdjustViewer;
  private NumberScalarWheelEditor  traPhaseAdjustSetter;

  private LabelViewer              phaAmpLabel;
  private SimpleScalarViewer       phaAmpViewer;
  private NumberScalarWheelEditor  phaAmpSetter;
  private LabelViewer              phaPhaseLabel;
  private SimpleScalarViewer       phaPhaseViewer;
  private NumberScalarWheelEditor  phaPhaseSetter;
  private LabelViewer              phaGainMagLabel;
  private SimpleScalarViewer       phaGainMagViewer;
  private NumberScalarWheelEditor  phaGainMagSetter;
  private LabelViewer              phaGainPhaLabel;
  private SimpleScalarViewer       phaGainPhaViewer;
  private NumberScalarWheelEditor  phaGainPhaSetter;
  private LabelViewer              phaTriggerLabel;
  private BooleanScalarComboEditor phaTriggerSetter;

  private LabelViewer              phaOffsetLabel;
  private SimpleScalarViewer       phaOffsetViewer;
  private NumberScalarWheelEditor  phaOffsetSetter;
  private LabelViewer              initPositionLabel;
  private SimpleScalarViewer       initPositionViewer;
  private NumberScalarWheelEditor  initPositionSetter;

  private JButton dismissBtn;
  private ErrorHistory errWin;

  public SRTuningDialog(ErrorHistory errWin,String traName) {

    this.traName = traName;
    this.traIdx = traName.substring(traName.length()-1);
    this.cavName = "cav1" + traIdx;
    this.errWin = errWin;

    attList = new AttributeList();
    attList.addErrorListener(errWin);

    JPanel globalPanel = new JPanel();
    globalPanel.setLayout(new BorderLayout());

    JPanel innerPanel = new JPanel();
    innerPanel.setLayout(null);
    innerPanel.setPreferredSize(new Dimension(450,595));

    JScrollPane scrollPane = new JScrollPane(innerPanel);
    globalPanel.add(scrollPane,BorderLayout.CENTER);

    // Transmitter ------------------------------------------------------------------------------------

    JPanel traPanel = new JPanel();
    traPanel.setLayout(null);
    traPanel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Transmitter",
                        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                        borderFont, Color.BLACK) );
    traPanel.setBounds(10,5,440,260);
    innerPanel.add(traPanel);

    traPinAttLabel = new LabelViewer();
    traPinAttLabel.setBackground(innerPanel.getBackground());
    traPinAttLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traPinAttLabel.setBounds(10,20,200,30);
    traPanel.add(traPinAttLabel);
    traPinAttViewer = new SimpleScalarViewer();
    traPinAttViewer.setFont(viewerFont);
    traPinAttViewer.setBackground(innerPanel.getBackground());
    traPinAttViewer.setAlarmEnabled(false);
    traPinAttViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traPinAttViewer.setBounds(210,18,100,35);
    traPanel.add(traPinAttViewer);
    traPinAttSetter = new NumberScalarWheelEditor();
    traPinAttSetter.setBackground(innerPanel.getBackground());
    traPinAttSetter.setFont(setterFont);
    traPinAttSetter.setBounds(310,15,120,40);
    traPanel.add(traPinAttSetter);
    traAmpRFLowLabel = new LabelViewer();
    traAmpRFLowLabel.setBackground(innerPanel.getBackground());
    traAmpRFLowLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traAmpRFLowLabel.setBounds(10,60,200,30);
    traPanel.add(traAmpRFLowLabel);
    traAmpRFLowViewer = new SimpleScalarViewer();
    traAmpRFLowViewer.setFont(viewerFont);
    traAmpRFLowViewer.setBackground(innerPanel.getBackground());
    traAmpRFLowViewer.setAlarmEnabled(false);
    traAmpRFLowViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traAmpRFLowViewer.setBounds(210,58,100,35);
    traPanel.add(traAmpRFLowViewer);
    traAmpRFLowSetter = new NumberScalarWheelEditor();
    traAmpRFLowSetter.setBackground(innerPanel.getBackground());
    traAmpRFLowSetter.setFont(setterFont);
    traAmpRFLowSetter.setBounds(310,55,120,40);
    traPanel.add(traAmpRFLowSetter);
    traPhaseRFLowLabel = new LabelViewer();
    traPhaseRFLowLabel.setBackground(innerPanel.getBackground());
    traPhaseRFLowLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traPhaseRFLowLabel.setBounds(10,100,200,30);
    traPanel.add(traPhaseRFLowLabel);
    traPhaseRFLowViewer = new SimpleScalarViewer();
    traPhaseRFLowViewer.setFont(viewerFont);
    traPhaseRFLowViewer.setBackground(innerPanel.getBackground());
    traPhaseRFLowViewer.setAlarmEnabled(false);
    traPhaseRFLowViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traPhaseRFLowViewer.setBounds(210,98,100,35);
    traPanel.add(traPhaseRFLowViewer);
    traPhaseRFLowSetter = new NumberScalarWheelEditor();
    traPhaseRFLowSetter.setBackground(innerPanel.getBackground());
    traPhaseRFLowSetter.setFont(setterFont);
    traPhaseRFLowSetter.setBounds(310,95,120,40);
    traPanel.add(traPhaseRFLowSetter);

    traRegulationONLabel = new LabelViewer();
    traRegulationONLabel.setBackground(innerPanel.getBackground());
    traRegulationONLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traRegulationONLabel.setBounds(10,140,200,30);
    traPanel.add(traRegulationONLabel);
    traRegulationONSetter = new BooleanScalarComboEditor();
    traRegulationONSetter.setBackground(innerPanel.getBackground());
    traRegulationONSetter.setBounds(210,140,120,30);
    traPanel.add(traRegulationONSetter);
    
    traDistribPinAttLabel = new LabelViewer();
    traDistribPinAttLabel.setBackground(getBackground());
    traDistribPinAttLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traDistribPinAttLabel.setBounds(10,180,200,30);
    traPanel.add(traDistribPinAttLabel);
    traDistribPinAttViewer = new SimpleScalarViewer();
    traDistribPinAttViewer.setFont(viewerFont);
    traDistribPinAttViewer.setBackground(getBackground());
    traDistribPinAttViewer.setAlarmEnabled(false);
    traDistribPinAttViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traDistribPinAttViewer.setBounds(210,178,120,35);
    traPanel.add(traDistribPinAttViewer);
    traDistribPinAttSetter = new NumberScalarWheelEditor();
    traDistribPinAttSetter.setBackground(getBackground());
    traDistribPinAttSetter.setFont(setterFont);
    traDistribPinAttSetter.setBounds(310,175,120,40);
    traPanel.add(traDistribPinAttSetter);
    
    traPhaseAdjustLabel = new LabelViewer();
    traPhaseAdjustLabel.setBackground(getBackground());
    traPhaseAdjustLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    traPhaseAdjustLabel.setBounds(10,220,200,30);
    traPanel.add(traPhaseAdjustLabel);
    traPhaseAdjustViewer = new SimpleScalarViewer();
    traPhaseAdjustViewer.setFont(viewerFont);
    traPhaseAdjustViewer.setBackground(getBackground());
    traPhaseAdjustViewer.setAlarmEnabled(false);
    traPhaseAdjustViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    traPhaseAdjustViewer.setBounds(210,218,120,35);
    traPanel.add(traPhaseAdjustViewer);
    traPhaseAdjustSetter = new NumberScalarWheelEditor();
    traPhaseAdjustSetter.setBackground(getBackground());
    traPhaseAdjustSetter.setFont(setterFont);
    traPhaseAdjustSetter.setBounds(310,215,120,40);
    traPanel.add(traPhaseAdjustSetter);

    // driver loops ------------------------------------------------------------------------------------

    JPanel phaPanel = new JPanel();
    phaPanel.setLayout(null);
    phaPanel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Driver regulation Loops",
                        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                        borderFont, Color.BLACK) );
    phaPanel.setBounds(10,265,440,220);
    innerPanel.add(phaPanel);

    phaAmpLabel = new LabelViewer();
    phaAmpLabel.setBackground(innerPanel.getBackground());
    phaAmpLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaAmpLabel.setBounds(10,20,200,30);
    phaPanel.add(phaAmpLabel);
    phaAmpViewer = new SimpleScalarViewer();
    phaAmpViewer.setFont(viewerFont);
    phaAmpViewer.setBackground(innerPanel.getBackground());
    phaAmpViewer.setAlarmEnabled(false);
    phaAmpViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaAmpViewer.setBounds(210,18,100,35);
    phaPanel.add(phaAmpViewer);
    phaAmpSetter = new NumberScalarWheelEditor();
    phaAmpSetter.setBackground(innerPanel.getBackground());
    phaAmpSetter.setFont(setterFont);
    phaAmpSetter.setBounds(310,15,120,40);
    phaPanel.add(phaAmpSetter);
    phaPhaseLabel = new LabelViewer();
    phaPhaseLabel.setBackground(innerPanel.getBackground());
    phaPhaseLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaPhaseLabel.setBounds(10,60,200,30);
    phaPanel.add(phaPhaseLabel);
    phaPhaseViewer = new SimpleScalarViewer();
    phaPhaseViewer.setFont(viewerFont);
    phaPhaseViewer.setBackground(innerPanel.getBackground());
    phaPhaseViewer.setAlarmEnabled(false);
    phaPhaseViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaPhaseViewer.setBounds(210,58,100,35);
    phaPanel.add(phaPhaseViewer);
    phaPhaseSetter = new NumberScalarWheelEditor();
    phaPhaseSetter.setBackground(innerPanel.getBackground());
    phaPhaseSetter.setFont(setterFont);
    phaPhaseSetter.setBounds(310,55,120,40);
    phaPanel.add(phaPhaseSetter);
    phaGainMagLabel = new LabelViewer();
    phaGainMagLabel.setBackground(innerPanel.getBackground());
    phaGainMagLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaGainMagLabel.setBounds(10, 100, 200, 30);
    phaPanel.add(phaGainMagLabel);
    phaGainMagViewer = new SimpleScalarViewer();
    phaGainMagViewer.setFont(viewerFont);
    phaGainMagViewer.setBackground(innerPanel.getBackground());
    phaGainMagViewer.setAlarmEnabled(false);
    phaGainMagViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaGainMagViewer.setBounds(210, 98, 100, 35);
    phaPanel.add(phaGainMagViewer);
    phaGainMagSetter = new NumberScalarWheelEditor();
    phaGainMagSetter.setBackground(innerPanel.getBackground());
    phaGainMagSetter.setFont(setterFont);
    phaGainMagSetter.setBounds(310, 95, 120, 40);
    phaPanel.add(phaGainMagSetter);
    phaGainPhaLabel = new LabelViewer();
    phaGainPhaLabel.setBackground(innerPanel.getBackground());
    phaGainPhaLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaGainPhaLabel.setBounds(10, 140, 200, 30);
    phaPanel.add(phaGainPhaLabel);
    phaGainPhaViewer = new SimpleScalarViewer();
    phaGainPhaViewer.setFont(viewerFont);
    phaGainPhaViewer.setBackground(innerPanel.getBackground());
    phaGainPhaViewer.setAlarmEnabled(false);
    phaGainPhaViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaGainPhaViewer.setBounds(210, 138, 100, 35);
    phaPanel.add(phaGainPhaViewer);
    phaGainPhaSetter = new NumberScalarWheelEditor();
    phaGainPhaSetter.setBackground(innerPanel.getBackground());
    phaGainPhaSetter.setFont(setterFont);
    phaGainPhaSetter.setBounds(310, 135, 120, 40);
    phaPanel.add(phaGainPhaSetter);

    phaTriggerLabel = new LabelViewer();
    phaTriggerLabel.setBackground(innerPanel.getBackground());
    phaTriggerLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaTriggerLabel.setBounds(10,180,200,30);
    phaPanel.add(phaTriggerLabel);
    phaTriggerSetter = new BooleanScalarComboEditor();
    phaTriggerSetter.setBackground(innerPanel.getBackground());
    phaTriggerSetter.setFont(viewerFont);
    phaTriggerSetter.setBounds(210,175,120,30);
    phaPanel.add(phaTriggerSetter);

    // tuner loops ------------------------------------------------------------------------------------

    JPanel tunPanel = new JPanel();
    tunPanel.setLayout(null);
    tunPanel.setBorder( BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "Tuner regulation Loop",
                        TitledBorder.LEFT, TitledBorder.DEFAULT_POSITION,
                        borderFont, Color.BLACK) );
    tunPanel.setBounds(10,490,440,100);
    innerPanel.add(tunPanel);

    phaOffsetLabel = new LabelViewer();
    phaOffsetLabel.setBackground(innerPanel.getBackground());
    phaOffsetLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    phaOffsetLabel.setBounds(10,20,200,30);
    tunPanel.add(phaOffsetLabel);
    phaOffsetViewer = new SimpleScalarViewer();
    phaOffsetViewer.setFont(viewerFont);
    phaOffsetViewer.setBackground(innerPanel.getBackground());
    phaOffsetViewer.setAlarmEnabled(false);
    phaOffsetViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    phaOffsetViewer.setBounds(210,18,100,35);
    tunPanel.add(phaOffsetViewer);
    phaOffsetSetter = new NumberScalarWheelEditor();
    phaOffsetSetter.setBackground(innerPanel.getBackground());
    phaOffsetSetter.setFont(setterFont);
    phaOffsetSetter.setBounds(310,15,120,40);
    tunPanel.add(phaOffsetSetter);

    initPositionLabel = new LabelViewer();
    initPositionLabel.setBackground(innerPanel.getBackground());
    initPositionLabel.setHorizontalAlignment(LabelViewer.LEFT_ALIGNMENT);
    initPositionLabel.setBounds(10,60,200,30);
    tunPanel.add(initPositionLabel);
    initPositionViewer = new SimpleScalarViewer();
    initPositionViewer.setFont(viewerFont);
    initPositionViewer.setBackground(innerPanel.getBackground());
    initPositionViewer.setAlarmEnabled(false);
    initPositionViewer.setBorder(BorderFactory.createLoweredBevelBorder());
    initPositionViewer.setBounds(210,58,100,35);
    tunPanel.add(initPositionViewer);
    initPositionSetter = new NumberScalarWheelEditor();
    initPositionSetter.setBackground(innerPanel.getBackground());
    initPositionSetter.setFont(setterFont);
    initPositionSetter.setBounds(310,55,120,40);
    tunPanel.add(initPositionSetter);

    try {

      NumberScalar traPinAtt = (NumberScalar)attList.add("srrf/ssa-tra/" + traName +"/PinAtt_Nominal");
      traPinAttLabel.setModel(traPinAtt);
      traPinAttViewer.setModel(traPinAtt);
      traPinAttSetter.setModel(traPinAtt);

      NumberScalar traAmpRFLow = (NumberScalar)attList.add("srrf/ssa-tra/" + traName +"/Amplitude_RFLow");
      traAmpRFLowLabel.setModel(traAmpRFLow);
      traAmpRFLowViewer.setModel(traAmpRFLow);
      traAmpRFLowSetter.setModel(traAmpRFLow);

      NumberScalar traPhaseRFLow = (NumberScalar)attList.add("srrf/ssa-tra/" + traName +"/Phase_RFLow");
      traPhaseRFLowLabel.setModel(traPhaseRFLow);
      traPhaseRFLowViewer.setModel(traPhaseRFLow);
      traPhaseRFLowSetter.setModel(traPhaseRFLow);

      BooleanScalar regulation_ON = (BooleanScalar)attList.add("srrf/ssa-tra/" + traName +"/Regulation_ON");
      traRegulationONLabel.setModel(regulation_ON);
      traRegulationONSetter.setAttModel(regulation_ON);
      
      NumberScalar traDistribPinAtt = (NumberScalar)attList.add("srrf/ssa-pinatt/c25/Attenuation" + traIdx);
      traDistribPinAttLabel.setModel(traDistribPinAtt);
      traDistribPinAttViewer.setModel(traDistribPinAtt);
      traDistribPinAttSetter.setModel(traDistribPinAtt);

      NumberScalar traPhaseAdjust = (NumberScalar)attList.add("srrf/ssa-tra/c25-phase/Phase_SSA_" + traIdx);
      traPhaseAdjustLabel.setModel(traPhaseAdjust);
      traPhaseAdjustViewer.setModel(traPhaseAdjust);
      traPhaseAdjustSetter.setModel(traPhaseAdjust);

      NumberScalar phaAmp = (NumberScalar)attList.add("srrf/ssa-driver/" + traName +"/RFAmplitude");
      phaAmpLabel.setModel(phaAmp);
      NumberScalar phaPhaV = (NumberScalar)attList.add("srrf/cav-adc/" + cavName + "/Cavity_Voltage");
      phaAmpViewer.setModel(phaPhaV);
      phaAmpSetter.setModel(phaAmp);

      NumberScalar phaPha = (NumberScalar)attList.add("srrf/ssa-driver/" + traName +"/RFPhase");
      phaPhaseLabel.setModel(phaPha);
      NumberScalar phaPhaP = (NumberScalar)attList.add("srrf/cav-adc/" + cavName + "/Phase_Cavity_1");
      phaPhaseViewer.setModel(phaPhaP);
      phaPhaseSetter.setModel(phaPha);

      NumberScalar phaGainI = (NumberScalar)attList.add("srrf/ssa-driver/" + traName +"/MagLoopGain");
      phaGainPhaLabel.setModel(phaGainI);
      phaGainPhaViewer.setModel(phaGainI);
      phaGainPhaSetter.setModel(phaGainI);

      NumberScalar phaGainP = (NumberScalar)attList.add("srrf/ssa-driver/" + traName +"/PhaseLoopGain");
      phaGainMagLabel.setModel(phaGainP);
      phaGainMagViewer.setModel(phaGainP);
      phaGainMagSetter.setModel(phaGainP);

      BooleanScalar phaTrigger = (BooleanScalar)attList.add("srrf/ssa-driver/" + traName +"/TriggerMode");
      phaTriggerLabel.setModel(phaTrigger);
      phaTriggerSetter.setAttModel(phaTrigger);

      NumberScalar phaOffset = (NumberScalar)attList.add("srrf/cav-tunerloop/" + cavName +"/TuningAngle");
      phaOffsetLabel.setModel(phaOffset);
      phaOffsetViewer.setModel(phaOffset);
      phaOffsetSetter.setModel(phaOffset);

      NumberScalar initPos = (NumberScalar)attList.add("srrf/cav-tunerloop/" + cavName +"/InitMotorPosition");
      initPositionLabel.setModel(initPos);
      initPositionViewer.setModel(initPos);
      initPositionSetter.setModel(initPos);

    } catch(ConnectionException e) {
      System.out.println(e.getMessage());
    }

    JPanel btnPanel = new JPanel();
    btnPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    globalPanel.add(btnPanel,BorderLayout.SOUTH);
    dismissBtn = new JButton("Dismiss");
    dismissBtn.addActionListener(this);
    btnPanel.add(dismissBtn);
            
    attList.startRefresher();

    setContentPane(globalPanel);
    setTitle("Tuning panel (" + traName.toUpperCase() + ")");

  }

  public void actionPerformed(ActionEvent e) {

    Object src = e.getSource();

    if( src==dismissBtn ) {
      setVisible(false);
    }

  }

  public void clearModel() {

    attList.removeErrorListener(errWin);
    attList.stopRefresher();

    traPinAttLabel.setModel(null);
    traPinAttViewer.clearModel();
    traPinAttSetter.setModel(null);
    traAmpRFLowLabel.setModel(null);
    traAmpRFLowViewer.clearModel();
    traAmpRFLowSetter.setModel(null);
    traPhaseRFLowLabel.setModel(null);
    traPhaseRFLowViewer.clearModel();
    traPhaseRFLowSetter.setModel(null);
    traRegulationONLabel.setModel(null);
    traRegulationONSetter.setModel(null);

    phaAmpLabel.setModel(null);
    phaAmpViewer.clearModel();
    phaAmpSetter.setModel(null);
    phaPhaseLabel.setModel(null);
    phaPhaseViewer.clearModel();
    phaPhaseSetter.setModel(null);
    phaGainPhaLabel.setModel(null);
    phaGainPhaViewer.clearModel();
    phaGainPhaSetter.setModel(null);
    phaGainMagLabel.setModel(null);
    phaGainMagViewer.clearModel();
    phaGainMagSetter.setModel(null);
    phaTriggerLabel.setModel(null);
    phaTriggerSetter.setModel(null);

    phaOffsetLabel.setModel(null);
    phaOffsetViewer.clearModel();
    phaOffsetSetter.setModel(null);

    initPositionLabel.setModel(null);
    initPositionViewer.clearModel();
    initPositionSetter.setModel(null);

  }

}
